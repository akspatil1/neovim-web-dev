# Neovim configurations for Web/UI development
This contains the most latest and easy configuration for Neovim with `Lazy.vim` and several other latest plugins, that will help you achieve the quality of VSCode IDE without the overhead and lag.


![Screenshot](screenshot.png)
![Screenshot](screenshot_code_suggesstion.png)
![Screenshot](screenshot_hover.png)
![Screenshot](screenshot_git_diff.png)

** *Note:Mind you need to know the basics of Vim first. Understanding and practicing the Vim keybindings(This is a journey, that can be acquired overtime) will help set yourself free of using mouse. 
With decreased UI interactions and mostly dealing with text editing, you can actually be more faster in productivity. And not to mention there is an ocean of 
configurations for Neovim and can be customly built to support any language/framewok you name it* **


To install NeoVim follow instuctions as per [NeoVim installation guide](https://github.com/neovim/neovim/blob/master/INSTALL.md). Or jsut copy paste below in terminal:

```
curl -LO https://github.com/neovim/neovim/releases/download/nightly/nvim-macos.tar.gz
tar xzf nvim-macos.tar.gz
./nvim-macos/bin/nvim
```


With the above I also recommend installing iTerm2 which is a nice bash prompt a little better than stock terminal: [iTerm2](https://iterm2.com/downloads.html)


## Key-bindings:

#### Factory bindings: [Link to Vim keybindings](https://devhints.io/vim) 

### File/Folder actions:
1. `Ctrl + s` Save file
2. `Ctrl + l` Lint file (Mind this uses the local config eslintrc and prettierrc from your directory)
3. `Ctrl + p` Open file search
4. `Ctrl + g` Live Grep through respository
5. `Ctrl + b` Open current buffers
6. `<space> + d` Open git history for current file 
7. `<space> + dd` Close git history
8. `<space> + gd` Open merge conflict viewer
9. `<space> + gdx` Open merge conflict viewer with x layout

### Code actions 
1. `Shift + K` Open the definition (This is equivalent to hovering onto a function/variable in VSCode and it pops up and small snippet of definition)
2. `Shift + F` Finds and goes to the definition, opens the file in a new buffer (equivalent to `cmd/ctrl + <click on the function/variable>` in VSCode)
3. `Shift + R` Finds all references/usage of the function/variable (This is equivalent to `Shift + F12` in VSCode)
4. `Ctrl + a` Code action/suggesstion for incomplete/wrong code detected by LSP (equivalent to hitting on the yellow/blue bubble in VSCode)
5. `gc` - Toggles the region using linewise comment
6. `gb` - Toggles the region using blockwise comment
7. `/ + enter or Shift+*` start find the selected text in file 
8. `<space>+l` Stop find the selected text in file 
9. `Ctrl + c` In `--VISUAL--` mode will copy selected text to system clipboard(So that you can paste it in your browser or elsewhere other than vim)
10. `<space>+tb` Toggle git line blamer
11. `<space>+hb` Open git line blamer in full view 
12. `<space>+hs` Stage hunk 
13. `<space>+hr` Reset hunk 
14. `<space>+hp` Preview hunk 
15. `<space>+hu` Undo Stage hunk 

### Window/Tab management 
1. `Ctrl + <back-tick>` Opens a terminal on right side. You can quit it by simply typing exit. Remember it acts as a vim editor an need to press `i or a` to enter --INSERT-- mode
2. `Ctrl+Shift+wv` Opens a file in buffer with split tab, where the window is split vertically 
3. `Ctrl+Shift+ws` Opens a file in buffer with split tab, where the window is split horizontally 
4. `<space>+p` Open file tree system to the left 
5. `<space>+pp` Close the file tree system to the left

### REST HTTP REQUEST'S

Create a file with .http extension (`example.http`). And place the cursor over the request line and press:
1. `<space> + r` Run the request 
2. `<space> + rp` Preview the request cURL command 
3. `<space> + rl` Re-run the last request

*Example.http:* 
```
POST https://reqres.in/api/users 
Content-Type: application-json 
{
    "userid": "{{$uuid}}"
    "time": "{{$timestamp}}"
}

POST https://reqres.in/api/users 
Content-type: application-json

< ./user.json

####

GET https://reqres.in/api/users?page=5
```
