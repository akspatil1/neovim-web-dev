vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")

vim.g.mapleader = " "
vim.keymap.set("n", "<C-s>", ":wa<CR>", {})
vim.keymap.set("v", "<C-c>", '"+y',{})
