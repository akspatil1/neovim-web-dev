return {
	"NeogitOrg/neogit",
	config = function()
		local neogit = require("neogit")
		neogit.setup({
			signs = {
				-- { CLOSED, OPENED }
				section = { "", "" },
				item = { "", "" },
				hunk = { "", "" },
			},
			integrations = { diffview = true }, -- adds integration with diffview.nvim
		})
	end,
}
