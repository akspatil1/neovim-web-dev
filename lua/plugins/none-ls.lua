return {
	"nvimtools/none-ls.nvim",
	config = function()
		local null_ls = require("null-ls")
		null_ls.setup({
			sources = {
				null_ls.builtins.formatting.stylua,
				null_ls.builtins.formatting.prettier,
				null_ls.builtins.formatting.eslint_d,
				null_ls.builtins.diagnostics.eslint_d,
				null_ls.builtins.completion.spell,
			},
		})
		-- Autoformat on save
		vim.cmd[[autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()]]
		vim.keymap.set("n", "<C-l>", vim.lsp.buf.format, {})
	end,
}
