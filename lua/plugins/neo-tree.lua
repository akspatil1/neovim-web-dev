return {
        {
                "akinsho/bufferline.nvim",
                version = "*",
        },
        {
                "nvim-neo-tree/neo-tree.nvim",
                branch = "v3.x",
                dependencies = {
                        "nvim-lua/plenary.nvim",
                        "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
                        "MunifTanjim/nui.nvim",
                },

                config = function()
                        local neotree = require("neo-tree")
                        local bufferline = require("bufferline")
                        bufferline.setup({
                                options = {
                                        mode = "buffers",
                                        diagnostics = "nvim_lsp",
                                        separator_style = "thick",
                                        hover = {
                                                enabled = true,
                                                delay = 200,
                                                reveal = { "close" },
                                        },
                                        offsets = {
                                                {
                                                        filetype = "neo-tree",
                                                        text = "File Explorer",
                                                        highlight = "Directory",
                                                        separator = true,
                                                },
                                        },
                                },
                        })
                        neotree.setup({
                                filesystem = {
                                        follow_current_file = {
                                                enabled = true,
                                                leave_dirs_open = false,
                                        },
                                        filtered_items = {
                                                visible = true,
                                                show_hidden_count = true,
                                                hide_dotfiles = false,
                                                hide_gitignored = false,
                                                changes = false,
                                        },
                                },
                                source_selector = {
                                        winbar = true,
                                        statusline = false,
                                },
                                git_status = {
                                        window = {
                                                position = "float",
                                                mappings = {
                                                        ["A"] = "git_add_all",
                                                        ["gu"] = "git_unstage_file",
                                                        ["ga"] = "git_add_file",
                                                        ["gr"] = "git_revert_file",
                                                        ["gc"] = "git_commit",
                                                        ["gp"] = "git_push",
                                                        ["gg"] = "git_commit_and_push",
                                                        ["o"] = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
                                                        ["oc"] = { "order_by_created", nowait = false },
                                                        ["od"] = { "order_by_diagnostics", nowait = false },
                                                        ["om"] = { "order_by_modified", nowait = false },
                                                        ["on"] = { "order_by_name", nowait = false },
                                                        ["os"] = { "order_by_size", nowait = false },
                                                        ["ot"] = { "order_by_type", nowait = false },
                                                },
                                        },
                                },
                        })
                        vim.keymap.set("n", "<leader>p", ":Neotree filesystem reveal left<CR>", {})
                        vim.keymap.set("n", "<leader>pp", ":Neotree filesystem close<CR>", {})
                end,
        },
}
