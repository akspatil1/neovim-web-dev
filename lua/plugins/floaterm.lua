return {
	"voldikss/vim-floaterm",
	config = function()
		vim.keymap.set("n", "<C-`>", ":FloatermNew --wintype=vsplit --position=right --width=0.2<CR>", {})
	end,
}
